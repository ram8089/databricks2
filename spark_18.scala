// Databricks notebook source
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.types.StructType
import org.graphframes._
import spark.implicits._

// COMMAND ----------

case class Airport(id: String, city: String) extends Serializable 

// COMMAND ----------

val airports = Array(Airport("SFO","San Francisco"),Airport("ORD","Chicago"),Airport("DFW","Dallas Fort Worth"))
val vertices = spark.createDataset(airports).toDF()
vertices.show()

// COMMAND ----------

case class Flight(id: String, src: String, dest: String, dst: Double, delay: Double) 
val flights = Array(Flight("SFO_ORD_2017-01-01_AA","SFO","ORD",1800, 40),Flight("ORD_DFW_2017-01-01_UA","ORD","DFW",800, 0),Flight("DFW_SFO_2017-01-01_DL","DFW","SFO",1400, 10))

val edges = spark.createDataset(flights).toDF
edges.show

// COMMAND ----------

val graph = GraphFrame(vertices, edges)
graph.vertices.show()

// COMMAND ----------

//how many airports are there
graph.vertices.count()

// COMMAND ----------

//how many flights are tehere
graph.edges.count()

// COMMAND ----------

// triplets = src edge dst

graph.triplets.show

// COMMAND ----------

graph.edges.filter("dst > 800").show

// COMMAND ----------

graph.edges.groupBy("src","dest").max("dst").sort(desc("max(dst)")).show

// COMMAND ----------

sc.parallelize(1 to 9, 3).map(x=>(x, "Hello")).collect

// COMMAND ----------

sc.parallelize(1 to 9, 3).mapPartitions(x=>((Array("Hello").iterator))).collect

// COMMAND ----------

sc.parallelize(1 to 9, 3).mapPartitions(x=>(List(x.next).iterator)).collect

// COMMAND ----------

 sc.parallelize(1 to 9, 3).mapPartitions(x=>(List(x.next,x.hasNext, "|").iterator)).collect

// COMMAND ----------

sc.parallelize(1 to 20, 4).takeOrdered(4)

// COMMAND ----------

sc.parallelize(Array("Apple","Banana","Grapes","Oranges","Grapes","Banana")).map(k=>(k,1)).countByKey()

// COMMAND ----------

 sc.parallelize("Hello").foreach(x=>println(x))

// COMMAND ----------

val rdd = sc.parallelize(

      Seq(

      ("One", Array(1,1,1,1,1,1,1)),

      ("Two", Array(2,2,2,2,2,2,2)),

      ("Three", Array(3,3,3,3,3,3))

      ) )

// COMMAND ----------

val df1 = spark.createDataFrame(rdd)

// COMMAND ----------

df1.rdd.partitions.size

// COMMAND ----------

df1.show()

// COMMAND ----------

df1.groupBy('_1 % 2).count()

// COMMAND ----------


df1.select(first(col("_2"))).show()

// COMMAND ----------

// MAGIC %sql SELECT floor(-0.1)

// COMMAND ----------

df1.printSchema

// COMMAND ----------

// MAGIC %sql SELECT regexp_replace('100-200', '(\\s+)', 'num')

// COMMAND ----------

.show

// COMMAND ----------

val df1 = spark.createDataFrame(rdd).toDF("Lable","Values")

// COMMAND ----------

df3 = spark[('David', 71), ('Angelica', 22), ('Martin', 7), ('Sol', 12)]

// COMMAND ----------

val myRdd = sc.parallelize(0 to 9).groupBy(_ % 2)


// COMMAND ----------

myRdd.dependencies

// COMMAND ----------

myRdd.toDebugString

// COMMAND ----------

val r1 = sc.parallelize(0 to 9)
val r3 = r1.map((_, 1))
r1.dependencies

// COMMAND ----------

r3.toDebugString

// COMMAND ----------

val hiveContext = new HiveContext(sparkContext)

wordCountsDstream.foreachRDD { rdd -> val wordCountsDataFrame = rdd.toDf("word","count") wordCountsDataFrame.registerTempTable("word_counts")}
HiveThriftServer2.startWithContext(hiveContext)

// COMMAND ----------

val dataset1 = spark.range(100)
dataset1.collect()

// COMMAND ----------

case class person (name: String, age: Long )

// COMMAND ----------

val df = spark.read.json('').as[person]

// COMMAND ----------

val someDF = Seq(
  (8, "bat"),
  (64, "mouse"),
  (-27, "horse")
).toDF("number", "word")

// COMMAND ----------

val someDF = spark.createDF(
  List(
    (8, "bat"),
    (64, "mouse"),
    (-27, "horse")
  ), List(
    ("number", IntegerType, true),
    ("word", StringType, true)
  )
)

// COMMAND ----------

//Create a DataFrame/Dataset from a collection (e.g. list or set)
case class Person(name: String, age: Long)

// COMMAND ----------

// MAGIC %fs ls /FileStore

// COMMAND ----------


val caseClassDS = Seq(Person(("Andy", 32)).toDS()
display(caseClassDS)

// COMMAND ----------

val caseClassDS = List(("Andy", 32),("Ram", 25)).toDF()

// COMMAND ----------

display(caseClassDS)

// COMMAND ----------

org.spark

// COMMAND ----------

val ran1 = spark.range(100)

// COMMAND ----------

case class IOTdata(
  battery_level: Long,
  c02_level: Long,
  cca2: String,
  cca3: String,
  cn: String,
  device_id: Long,
  device_name: String,
  humidity: Long,
  ip: String,
  latitude: Double,
  longitude: Double,
  scale: String,
  temp: Long,
  timestamp: Long
)

// COMMAND ----------

val ds= spark.read.json("/databricks-datasets/iot/iot_devices.json").as[IOTdata]

// COMMAND ----------

display(ds)

// COMMAND ----------

ds.take(10).foreach(println(_))

// COMMAND ----------

ds.createOrReplaceTempView("Dssql")

// COMMAND ----------

// MAGIC %sql select * from Dssql

// COMMAND ----------

// filter out all devices whose temperature exceed 25 degrees and generate
// another Dataset with three fields that of interest and then display
// the mapped Dataset

%sql 
select device_name from Dssql where temp > 25

// COMMAND ----------

val dstemp = ds.filter(d -> d.temp > 25).map(d -> (d.temp, d.device_name, d.cca3))
display(dstemp)

// COMMAND ----------

val firstDataFrame = sqlContext.range(1000000)


// COMMAND ----------

firstDataFrame.withColumn( "id", id*2).show()

// COMMAND ----------

firstDataFrame.storageLevel.get

// COMMAND ----------

case class hecourse(id : Int, name: String, fee : Int, Venue: String, duaration: Int)

// COMMAND ----------

val heDS1 = sc.parallelize(Seq(HECourse(1, "Hadoop", 6000, "Mumbai", 5),HECourse(2, "Spark", 5000, "Pune", 4),HECourse(3, "Python", 4000, "Hyderabad", 3)) , 3).toDS()


// COMMAND ----------

println(heDS1.rdd.partitions.size)

// COMMAND ----------


